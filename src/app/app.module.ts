import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardEffects } from './dashboard/dashboard.effects';
import { AppState, dashboardReducer } from './state';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot<AppState>({
      dashboard: dashboardReducer
    }),
    EffectsModule.forRoot([DashboardEffects]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
