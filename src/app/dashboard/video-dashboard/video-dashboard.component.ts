import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable, combineLatest } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Video } from 'src/app/common/interfaces';
import { AppState, getVideos } from 'src/app/state';
import { VideoDataService } from 'src/app/video-data.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent {
  videos: Observable<Video[]>;

  constructor(router: Router, ar: ActivatedRoute, store: Store<AppState>) {
    const ids: Observable<string> = ar.queryParams.pipe(
      map(params => params.selectedVideoId)
    );
    this.videos = combineLatest(store.pipe(select(getVideos)), ids).pipe(
      // use tap because this is a side effect
      tap(([videos, id]) => {
        if (!id && !!videos && videos.length) {
          // if there's not an existing selection, select the first video
          router.navigate(
            [], { queryParams: { selectedVideoId: videos[0].id } }
          );
        }
      }),
      map(([videos, _]) => videos)
    );
  }
}
