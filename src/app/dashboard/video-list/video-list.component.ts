import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Video } from 'src/app/common/interfaces';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent {
  @Input() videos: Video[] = [];
  selectedVideoId: Observable<string>;

  constructor(route: ActivatedRoute) {
    this.selectedVideoId = route.queryParams.pipe(
      map(params => params.selectedVideoId)
    );
  }
}
