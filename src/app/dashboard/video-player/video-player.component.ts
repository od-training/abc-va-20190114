import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, share, switchMap } from 'rxjs/operators';
import { Video } from 'src/app/common/interfaces';
import { VideoDataService } from 'src/app/video-data.service';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent {
  video: Observable<Video>;

  constructor(route: ActivatedRoute, svc: VideoDataService) {
    this.video = route.queryParams.pipe(
      map(params => params.selectedVideoId),
      switchMap(id => svc.getVideo(id)),
      share()
    );
  }
}
