import { Injectable } from '@angular/core';
import { Actions, Effect, ROOT_EFFECTS_INIT, ofType } from '@ngrx/effects';
import { map, switchMap, tap } from 'rxjs/operators';

import { VideoListArrivedAction } from '../state';
import { VideoDataService } from '../video-data.service';

@Injectable()
export class DashboardEffects {
  // To use effects we will always need the action stream injected; in
  // some cases it is also helpful to inject the Store itself, with a
  // parameter like:
  // private store: Store<AppState>
  constructor(
    private readonly actions$: Actions,
    private readonly svc: VideoDataService
  ) {
  }

  @Effect({dispatch: false})
  logging = this.actions$.pipe(
    tap(action => console.log({action}))
  );

  // ROOT_EFFECTS_INIT is a special action that is dispatched at the end of
  // NgRx's initialization process, so this effect executes at application
  // initialization.
  @Effect()
  init$ = this.actions$
    .pipe(
      ofType(ROOT_EFFECTS_INIT),
      switchMap(() => this.svc.load()),
      map(videos => new VideoListArrivedAction(videos))
    );

}
