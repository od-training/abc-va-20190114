import { Action, createFeatureSelector, createSelector } from '@ngrx/store';

import { Video } from './common/interfaces';

const VIDEO_LIST_ARRIVED = 'VIDEO_LIST_ARRIVED';
export class VideoListArrivedAction implements Action {
  type = VIDEO_LIST_ARRIVED;

  constructor(readonly payload: Video[]) { }
}

export interface DashboardState {
  videoList: Video[];
  name: string;
}

export interface AppState {
  dashboard: DashboardState;
}

const initialState: DashboardState = {
  videoList: [],
  name: '',
};

export function dashboardReducer(
  state: DashboardState = initialState,
  action: Action
): DashboardState {
  switch (action.type) {
    case VIDEO_LIST_ARRIVED:
      return {
        ...state,
        videoList: [...(action as VideoListArrivedAction).payload]
      };
    case ADD_VIDEO:
      return {
        ...state,
        videoList: [...state.videoList, action.payload]
      };
    case RESET:
      return {
        videoList: [],
        name: ''
      };
  }
  return state;
}

const getDashboardState =
  createFeatureSelector<DashboardState>('dashboard');

export const getVideos =
  createSelector(getDashboardState, state => state.videoList);

export const getName =
  createSelector(getDashboardState, state => state.name);

export const getAuthorVideos =
  createSelector(
    getVideos,
    getName,
    (videos, name) => videos.filter(v => v.author === name)
  );
