import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Video } from './common/interfaces';

const API_URL = 'https://api.angularbootcamp.com';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private http: HttpClient) { }

  load() {
    return this.http
      .get<Video[]>(API_URL + '/videos')
      .pipe(
        map(videos =>
          videos.filter(video =>
            video.title.startsWith('Angular')))
      );
  }

  getVideo(id: string): Observable<Video> {
    return this.http.get<Video>(API_URL + '/videos/' + id);
  }
}
