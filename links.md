# Angular Boot Camp
[Angular Boot Camp Curriculum](https://github.com/angularbootcamp/abc)

[Angular Boot Camp Zip File](http://angularbootcamp.com/abc.zip)

[Video Manager (target application)](http://videomanager.angularbootcamp.com)

[Workshop Repo](https://bitbucket.org/od-training/abc-va-20190114)

[Sample Videos Data](https://api.angularbootcamp.com/videos)

[Survey](https://angularbootcamp.com/survey)

# Resources
[TypeScript Playground](http://www.typescriptlang.org/play/)

[DOM Events](https://developer.mozilla.org/en-US/docs/Web/Events)

[Testing Blog
Post](https://developers.livechatinc.com/blog/angular-dependency-injection-components/)

[Jasmine](https://jasmine.github.io/2.9/introduction)

[Component Interaction in Angular](https://angular.io/guide/component-interaction)

[Kyle's talk on Managing State in Angular 2](https://www.youtube.com/watch?v=eBLTz8QRg4Q)

[Functional-Light JavaScript (free
ebook)](https://github.com/getify/Functional-Light-JS)

[Local storage with NgRx](https://github.com/btroncone/ngrx-store-localstorage)

[Zachary's NgRx store demo
app](https://bitbucket.org/zachary_kipping/ngrx-webstore/src/master/)

[Redux immutability tools](https://github.com/markerikson/redux-ecosystem-links/blob/master/immutable-data.md)

## VS Code extensions
[Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)

[Angular File Changer](https://marketplace.visualstudio.com/items?itemName=john-crowson.angular-file-changer)

[vscode-icons](https://github.com/vscode-icons/vscode-icons)

[multiple cursor case
preserve](https://github.com/Cardinal90/multi-cursor-case-preserve)

[Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)

## Date Libraries
[Moment](https://momentjs.com/)

[date-fns](https://date-fns.org/)

## RXJS Pipeable Operators Resources
  [RXMarbles](http://rxmarbles.com)
  [LearnRXJS](https://www.learnrxjs.io/)
  [Seven Operators to Get Started with RxJS (Article)](https://www.infoq.com/articles/rxjs-get-started-operators)
  [ngHouston Youtube Channel](https://www.youtube.com/channel/UC3cfVUpgrO5TK6cvH04ieSQ)
  ["I Switched a Map" video](https://www.youtube.com/watch?v=rUZ9CjcaCEw)
